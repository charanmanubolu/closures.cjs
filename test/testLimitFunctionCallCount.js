const limitFunctionCallCount = require('../limitFunctionCallCount.cjs')

function square(num) {
    return num * num
}

let limitcount = limitFunctionCallCount(square, 5)
console.log(limitcount(4))
console.log(limitcount(2))
console.log(limitcount(3))
console.log(limitcount(1))
console.log(limitcount(6))
console.log(limitcount(5))