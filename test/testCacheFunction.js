const cacheFunction=require('../cacheFunction.cjs')

function square(num){
    return num*num
}


let cache = cacheFunction(square)

console.log(cache(2))
console.log(cache(3))
console.log(cache(2))