
function cacheFunction(cb) {

    // Should return a function that invokes `cb`.
    // A cache (object) should be kept in closure scope.
    // The cache should keep track of all arguments have been used to invoke this function.
    // If the returned function is invoked with arguments that it has already seen
    // then it should return the cached result and not invoke `cb` again.
    // `cb` should only ever be invoked once for a given set of arguments.
    const cache = {};
    return function (...args) {
        try {
            let arguements = JSON.stringify(...args)
            for (let key in cache) {
                if (key == arguements) {
                    return cache[key]
                }
            }
            let result = cb(...args)
            cache[arguements] = result;
            return result
        } catch (err) {
            console.log(err)
        }
    }
}

module.exports = cacheFunction